﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyWebApplication.Models
{
    [Table("company",Schema = "public")]
    public class CompanyClass
    {
        [Key]
        public int companyid { get; set; }
        public string companyname { get; set; }

        public string email { get; set; }

        public string adress { get; set; }

    }
}