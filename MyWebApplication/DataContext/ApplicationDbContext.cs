﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using MyWebApplication.Models;

namespace MyWebApplication.DataContext
{
    public class ApplicationDbContext:DbContext
    {
        public ApplicationDbContext():base(nameOrConnectionString: "Myconnection")
        {
        
        }

        public virtual DbSet<CompanyClass>CompanyObj { get; set; }
    }
}