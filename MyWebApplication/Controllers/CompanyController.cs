﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyWebApplication.DataContext;
using MyWebApplication.Models;

namespace MyWebApplication.Controllers
{
    public class CompanyController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Company
        public ActionResult Index()
        {
            return View(db.CompanyObj.ToList());
        }

        // GET: Company/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompanyClass companyClass = db.CompanyObj.Find(id);
            if (companyClass == null)
            {
                return HttpNotFound();
            }
            return View(companyClass);
        }

        // GET: Company/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Company/Create
        // Aşırı gönderim saldırılarından korunmak için bağlamak istediğiniz belirli özellikleri etkinleştirin. 
        // Daha fazla bilgi için bkz. https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "companyid,companyname,email,adress")] CompanyClass companyClass)
        {
            if (ModelState.IsValid)
            {
                db.CompanyObj.Add(companyClass);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(companyClass);
        }

        // GET: Company/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompanyClass companyClass = db.CompanyObj.Find(id);
            if (companyClass == null)
            {
                return HttpNotFound();
            }
            return View(companyClass);
        }

        // POST: Company/Edit/5
        // Aşırı gönderim saldırılarından korunmak için bağlamak istediğiniz belirli özellikleri etkinleştirin. 
        // Daha fazla bilgi için bkz. https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "companyid,companyname,email,adress")] CompanyClass companyClass)
        {
            if (ModelState.IsValid)
            {
                db.Entry(companyClass).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(companyClass);
        }

        // GET: Company/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompanyClass companyClass = db.CompanyObj.Find(id);
            if (companyClass == null)
            {
                return HttpNotFound();
            }
            return View(companyClass);
        }

        // POST: Company/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CompanyClass companyClass = db.CompanyObj.Find(id);
            db.CompanyObj.Remove(companyClass);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
